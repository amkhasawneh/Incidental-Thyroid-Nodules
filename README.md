# Incidental-Thyroid-Nodules

In this study (PDF appended), we have examined the prevalence of incidental thyroid nodules (ITNs) in CT scans in the records of a university hospital. Here, I try to apply a logistic regression model to infer the correlation of age and sex with the existence (or lack) of an ITN. It does not seem to be working well, as the sensitivity is low.

I have also tried my hand at ggplot2, which made a nice-looking faceted scatterplot (also uploaded).
