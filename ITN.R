#Importing Dataset into Dataframe and Making Pertinent Conversions:
ITN <- read.csv("C:\\Users\\abdul\\Desktop\\R Programming\\Freestyle\\ITN\\ITN Patient List.csv")
ITN
summary(ITN)
ITN$Sex <- as.factor(ITN$Sex)
ITN$Age <- as.numeric(ITN$Age)
ITN$ITN <- as.factor(ITN$ITN)
ITN$Sides <- as.factor(ITN$Sides)

#Making a Faceted (by Sex) Scatter Plot of Lesion Size Against Age:
library(ggplot2)
ggplot(data = ITN[complete.cases(ITN),], aes(x = Age, y = Size, color = Sides)) +
  geom_point() +
  facet_grid(Sex~.) 

#Splitting the Dataset into a Training Set and a Data Set:
set.seed(0)
split <- sample(c(rep(0, 0.8*nrow(ITN)),
                  rep(1, 0.2*nrow(ITN))))
train <- ITN[split == 0,]
test <- ITN[split == 1,]

#Training the Logistic Regression Model (on the training set):
model <- glm(ITN ~ Sex + Age, data = train, family = binomial)

#Predicting the Test Set Results:
prediction <- predict(model, newdata = test, type = "response")


#Making a Confusion Matrix:
pred.model <- ifelse(prediction>0.5, "ITN", "NONE")
test$ITN <- ifelse(test$ITN == 1, "ITN", "NONE")
cm <- table(pred.model, test$ITN)
accuracy <- mean(pred.model == test$ITN)

#Summary:
Summary <- list("Coefficients" = summary(model)$coef, "Confusion Matrix" = cm, "Accuracy" = accuracy)
Summary